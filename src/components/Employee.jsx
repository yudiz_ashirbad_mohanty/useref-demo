import {useContext, useState, useEffect} from 'react';
import {EmployeeContext} from '../contexts/EmployeeContext';
import { Modal, Button } from 'react-bootstrap';
import EditForm from './EditForm'

const Employee = ({employee}) => {

    const {deleteEmployee} = useContext(EmployeeContext)
    const [show, setShow] = useState(false);
    const handleShow = () => setShow(true);
    const handleClose = () => setShow(false);

    useEffect(() => {
        handleClose()
    }, [employee])
    return (
        <>
            <td>{employee.name}</td>
            <td>{employee.email}</td>
            <td>{employee.address}</td>
            <td>{employee.phone}</td>
            <td>
              <button onClick={handleShow}>Edit</button>
               <button onClick={() => deleteEmployee(employee.id)}  className="" data-toggle="">Delete</button>
            </td>
            <Modal show={show} onHide={handleClose}>
        <Modal.Header>
            <Modal.Title>
                EDIT
            </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <EditForm theEmployee={employee} />
        </Modal.Body>
        <Modal.Footer>
                <Button variant="" onClick={handleClose}>
                    Close 
                </Button>
        </Modal.Footer>
    </Modal>
        </>
    )
}

export default Employee;