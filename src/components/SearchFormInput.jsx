
import React, { forwardRef } from "react";
const SearchFormInput = ( props, ref) => {
  return (
    <input
      type="text"
      placeholder="Search by Name"
      className="search"
      ref={ref}
    />
  )
}

export default  forwardRef (SearchFormInput);