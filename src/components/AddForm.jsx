import { Form, Button } from "react-bootstrap"
import {EmployeeContext} from '../contexts/EmployeeContext';
import {useContext, useRef,} from 'react';
const AddForm = () =>{
    const {addEmployee} = useContext(EmployeeContext);
    const name =useRef(null);
    const email =useRef(null);
    const phone =useRef(null);
    const address =useRef(null);
    const handleSubmit = (e) => {
        e.preventDefault();
        addEmployee({name:name.current.value,email:email.current.value,phone:phone.current.value,address:address.current.value});
        name.current.value="";
        email.current.value="";
        phone.currentvalue="";
        address.currentvalue="";
    }
     return (
        <Form onSubmit={handleSubmit}>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Name *"
                    name="name"
                    ref={name}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="email"
                    placeholder="Email *"
                    name="email"
                    ref={email}
                    required
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="textarea"
                    placeholder="Address"
                    rows={3}
                    name="address"
                    ref={address} 
                />
            </Form.Group>
            <Form.Group>
                <Form.Control
                    type="text"
                    placeholder="Phone"
                    name="phone"
                    ref={phone}
                />
            </Form.Group>
            <Button variant="success" type="submit" block>
                Add New Employee
            </Button>
           
        </Form>

     )
}

export default AddForm;