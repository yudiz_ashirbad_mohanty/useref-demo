import React,{useRef} from 'react'
import {EmployeeContext} from '../contexts/EmployeeContext';
import SearchFormInput from './SearchFormInput';
const SearchForm = () => {
    const {searchdata} =(EmployeeContext);
    const data =useRef(null);
    function handleSearchButton(e) {
        e.preventDefault();
        searchdata({ type: "FILTER_BY_SEARCH", payload: data.current.value });
        searchdata.current.value = "";
      }

  return (
    <form className="">
   <SearchFormInput ref={searchdata} />
    <button className="btn" onClick={handleSearchButton}>
      Search
    </button>
  </form>
  )
}

export default SearchForm